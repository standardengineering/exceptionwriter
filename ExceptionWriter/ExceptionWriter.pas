﻿//Copyright (C) 2010 by Standard Engineering
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

namespace Essy.Tools;

interface

uses
  System.Management,
  System.Collections.Generic,
  Microsoft.Win32;

type
  ExceptionWriter = public static class
  private
    method get_AssemblyVersion: String;
    method get_ComputerModel: String;
    method get_ComputerManufacturer: String;
    method get_OSName: String;
    method get_OSVersion: String;
    method get_NETFrameworkVersion: String;
    method get_VideoChipName: String;
    method get_AssemblyTitle: String;
    method get_CPUName: String;
    method get_InternetConnected: String;
    method get_ScreenResolution: String;
    property fDontShowContinueButton: Boolean := false;
  protected
  public
    class property HandleReadWriteExceptionAction: Action<String, Exception, String, String>; 
    property ApplicationSpecificLog: String;
    method HandleException(aException: Exception; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
    method HandleException(aException: Exception; aExtraInfoList: IList<ExceptionWriterExtraInfo>; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
    method HandleExceptionNoContinue(aException: Exception; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
    method HandleExceptionNoContinue(aException: Exception; aExtraInfoList: IList<ExceptionWriterExtraInfo>; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
    method CreateExceptionString(aException: Exception; aExtraInfoList: IList<ExceptionWriterExtraInfo>): String; virtual;
  end;

  TExceptionApplicationType = public enum (None, Storm, AIX1000);

  ExceptionWriterExtraInfo = public class
  private
  public
    property Name: String;
    property Value: String;
  end;

  ExceptionWriterWindow = public enum(None, CommandLine, Winforms, WPF, WPFMetro);

implementation

uses
  System.Net,
  System.Net.NetworkInformation,
  System.Reflection,
  System.IO,
  System.Windows,
  System.Windows.Forms.VisualStyles;

method ExceptionWriter.HandleExceptionNoContinue(aException: Exception; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
begin
  fDontShowContinueButton := true;
  result := HandleException(aException, nil, aFilePath, aOutputWindow);
end;

method ExceptionWriter.HandleException(aException: Exception; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
begin
  result := HandleException(aException, nil, aFilePath, aOutputWindow);
end;

method ExceptionWriter.HandleExceptionNoContinue(aException: Exception; aExtraInfoList: IList<ExceptionWriterExtraInfo>; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
begin
  fDontShowContinueButton := true;
  try
    result := HandleException(aException, aExtraInfoList, aFilePath, aOutputWindow);
  except
    on ex: Exception do
    begin
      if ex is InvalidOperationException then
      begin
        result := HandleException(aException, aExtraInfoList, aFilePath, ExceptionWriterWindow.None) // in case if there is an issue to set ExceptionWriterWindow.WPFMetro to the System.Windows.Window.Owner property
      end
      else raise;
    end;
  end;
end;

method ExceptionWriter.HandleException(aException: Exception; aExtraInfoList: IList<ExceptionWriterExtraInfo>; aFilePath: String; aOutputWindow: ExceptionWriterWindow) : Boolean;
begin
  if not assigned(aException) then exit true;
  var errorString := self.CreateExceptionString(aException, aExtraInfoList);
  var filePathToSave := aFilePath;
  var extensionNumber := 2;
  while (File.Exists(filePathToSave)) do
  begin
    var dirPath := Path.GetDirectoryName(filePathToSave);
    var fileName := Path.GetFileNameWithoutExtension(aFilePath);
    var newFileName := String.Format("{0}_{1}.txt",fileName , extensionNumber.ToString());
    filePathToSave := Path.Combine(dirPath, newFileName);
    inc(extensionNumber);
  end;
  try
    File.WriteAllText(filePathToSave, errorString);
  except
    on ex: Exception do
    begin
      var description := Essy.Tools.Properties.Resources.strAn_exception_has_occured_but_the_exception_log_file_could_not_be_saved_;
      var consequence := Essy.Tools.Properties.Resources.strContact_Technical_Services_if_the_problem_persists_;
      if assigned(HandleReadWriteExceptionAction) then HandleReadWriteExceptionAction.Invoke(filePathToSave, ex, description, consequence);
    end;
  end;

  case aOutputWindow of
    ExceptionWriterWindow.CommandLine:
    begin
      Console.WriteLine('********************************************************************');
      Console.WriteLine('The application has detected an unexpected problem...');
      Console.WriteLine(aException.Message);
      Console.WriteLine(System.Environment.NewLine);
      Console.WriteLine('The details of this error are written into this file:');
      Console.WriteLine(filePathToSave);
      Console.WriteLine(System.Environment.NewLine);
      Console.WriteLine('The application will shutdown. Please report this error immediately.');
      Console.WriteLine('********************************************************************');
      result := false;
    end;
    ExceptionWriterWindow.Winforms:
    begin
      using dlg := new InfoForm() do
      begin
        dlg.Topmost := true;
        var dlgResult := dlg.ShowDialog(aException.Message, filePathToSave, fDontShowContinueButton);
        result := (dlgResult = System.Windows.Forms.DialogResult.Ignore);
      end;
    end;
    ExceptionWriterWindow.WPF:
    begin
      using dlg := new InfoFormWPF() do
      begin
        dlg.Owner := Application:Current:MainWindow;
        dlg.Topmost := true;
        Application:Current:MainWindow:Opacity := 0.5;
        result := dlg.ShowDialog(aException.Message, filePathToSave, fDontShowContinueButton);
        Application:Current:MainWindow:Opacity := 1.0;
      end;
    end;
    ExceptionWriterWindow.WPFMetro:
    begin
      using dlg := new InfoFormMetro() do
      begin
        dlg.Owner := Application:Current:MainWindow;
        dlg.Topmost := true;
        Application:Current:MainWindow:Opacity := 0.5;
        result := dlg.ShowDialog(aException.Message, filePathToSave, fDontShowContinueButton);
        Application:Current:MainWindow:Opacity := 1.0;
      end;
    end;
  end;
end;

method ExceptionWriter.CreateExceptionString(aException: Exception; aExtraInfoList: IList<ExceptionWriterExtraInfo>): String;
begin
  var sb := new System.Text.StringBuilder;

  var writeExceptionDetails := method(ex: Exception)
  begin
    sb.Append('Type: ');
    sb.AppendLine(typeOf(ex).ToString);
    sb.Append('Message: ');
    sb.AppendLine(ex.Message);
    sb.Append('Source: ');
    sb.AppendLine(ex.Source);
    sb.AppendLine('Stack Trace: ');
    sb.AppendLine(ex.StackTrace);
  end;

  sb.Append('Application Title: ');
  sb.AppendLine(get_AssemblyTitle);
  sb.Append('Application Version: ');
  sb.AppendLine(get_AssemblyVersion);
  sb.Append('Time of error: ');
  sb.AppendLine(DateTime.Now.ToString);
  sb.AppendLine();
  sb.AppendLine('---Environment Information---');
  sb.Append('Operating System: ');
  sb.AppendLine(get_OSName());
  sb.Append('Operating System Version: ');
  sb.AppendLine(get_OSVersion());
  sb.AppendLine(iif(IntPtr.Size > 4, 'Application Mode: 64-bit','Application Mode: 32-bit'));
  sb.Append('CLR Version: ');
  sb.AppendLine(Environment.Version.ToString());
  sb.Append('.Net Framework Version: ');
  sb.AppendLine(get_NETFrameworkVersion());
  sb.Append('Internet Connected: ');
  sb.AppendLine(get_InternetConnected());
  sb.Append('Computer Manufacturer: ');
  sb.AppendLine(get_ComputerManufacturer());
  sb.Append('Computer Model: ');
  sb.AppendLine(get_ComputerModel());
  sb.Append('CPU: ');
  sb.AppendLine(get_CPUName());
  sb.Append('CPU Count: ');
  sb.AppendLine(Environment.ProcessorCount.ToString);
  sb.Append('Video Chip: ');
  sb.AppendLine(get_VideoChipName());
  sb.Append('Screen Resolution: ');
  sb.AppendLine(get_ScreenResolution());
  sb.Append('Total System Memory: ');
  sb.Append((new Microsoft.VisualBasic.Devices.ComputerInfo().TotalPhysicalMemory / 1048576).ToString());
  sb.AppendLine(' MB');
  sb.Append('Memory used by application: ');
  sb.Append((GC.GetTotalMemory(false) / 1048576.0).ToString('0.000'));
  sb.AppendLine(' MB');
  sb.Append('Culture Setting: ');
  sb.AppendLine(System.Globalization.CultureInfo.CurrentCulture.Name);
  sb.Append('Decimal Separator / Group Separator / List Separator: ');
  sb.Append(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
  sb.Append(' ');
  sb.Append(System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberGroupSeparator);
  sb.Append(' ');
  sb.AppendLine(System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator);
  sb.Append('Computer Name: ');
  sb.AppendLine(Environment.MachineName);
  sb.Append('User Name: ');
  sb.AppendLine(Environment.UserName);
  sb.AppendLine();

  if assigned(aExtraInfoList) then
  begin
    sb.AppendLine('---Instrument Information---');
    for each ei in aExtraInfoList do
    begin
      sb.Append(ei:Name);
      sb.Append(': ');
      sb.AppendLine(ei:Value);
    end;
    sb.AppendLine();
  end;

  if not String.IsNullOrWhiteSpace(ApplicationSpecificLog) then
  begin
    sb.Append(ApplicationSpecificLog);
  end;
  
  sb.AppendLine();
  sb.AppendLine('Root Exception:');
  writeExceptionDetails(aException);
  var innerEx := aException.InnerException;
  var counter := 1;
  while assigned(innerEx) do
  begin
    sb.AppendLine();
    sb.AppendLine(String.Format('Inner Exception {0}:', counter));
    writeExceptionDetails(innerEx);
    innerEx := innerEx.InnerException;
    inc(counter);
  end;
  result := sb.ToString();
end;

method ExceptionWriter.get_AssemblyTitle: String;
begin
  var attributes: array of Object := &Assembly.GetEntryAssembly.GetCustomAttributes(typeOf(AssemblyProductAttribute), false);
  if not assigned(attributes) then exit String.Empty;
  if (attributes.Length = 0) then
  begin
    exit String.Empty;
  end
  else
  begin
    exit (attributes[0] as AssemblyProductAttribute).Product;
  end;
end;

method ExceptionWriter.get_AssemblyVersion: String;
begin
  var ver :=  &Assembly.GetEntryAssembly.GetName.Version;
  if not assigned(ver) then exit String.Empty;
  result := ver.ToString + case ver.Build of
                                     0: ' [Alpha Build]';
                                     1: ' [Beta Build]';
                                     2: ' [Release Candidate]';
                                   else
                                     String.Empty;
                                   end;
end;

class method ExceptionWriter.get_CPUName: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select Name from Win32_Processor');
    var managementObjects := searcher.Get();
    for each mo: ManagementObject in managementObjects do
    begin
       exit mo['Name'].ToString();
    end;
  except
  end;
end;

class method ExceptionWriter.get_VideoChipName: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select Name from Win32_VideoController');
    var managementObjects := searcher.Get();
    var res := String.Empty;
    for each mo: ManagementObject in managementObjects index i do
    begin
       res := res + iif(i > 0, ' | ', '') + mo['Name'].ToString();
    end;
    exit res;
  except
  end;
end;

class method ExceptionWriter.get_OSName: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select Caption, OSArchitecture from Win32_OperatingSystem');
    var managementObjects := searcher.Get();
    for each mo: ManagementObject in managementObjects do
    begin
      var caption := mo['Caption'];
      var arch := mo['OSArchitecture'];
      result := caption.ToString();
      if assigned(arch) then result := result + ' (' + arch.ToString() + ')';
      exit;
    end;
  except
  end;
end;

class method ExceptionWriter.get_ComputerManufacturer: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select Manufacturer from Win32_ComputerSystem');
    var managementObjects := searcher.Get();
    for each mo: ManagementObject in managementObjects do
    begin
       exit mo['Manufacturer'].ToString();
    end;
  except
  end;
end;

class method ExceptionWriter.get_ComputerModel: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select Model from Win32_ComputerSystem');
    var managementObjects := searcher.Get();
    for each mo: ManagementObject in managementObjects do
    begin
       exit mo['Model'].ToString();
    end;
  except
  end;
end;

class method ExceptionWriter.get_InternetConnected: String;
begin
  result := 'No';
  try
    var client := new WebClient ();
    var response := client.DownloadString("http://www.google.com");
    result := 'Yes';
  except
    //do nothing
  end;
end;

method ExceptionWriter.get_OSVersion: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select Version, CSDVersion from Win32_OperatingSystem');
    var managementObjects := searcher.Get();
    for each mo: ManagementObject in managementObjects do
    begin
      var ver := mo['Version'];
      var csdversion := mo['CSDVersion'];
      result := ver.ToString();
      if assigned(csdversion) then result := result + ' (' + csdversion.ToString() + ')';
      exit;
    end;
  except
  end;
end;

method ExceptionWriter.get_NETFrameworkVersion: String;
begin

  // Checking the version using >= enables forward compatibility.
  // GSD apps target .NET Framework 4.6, so we want to know if 4.6 or
  // greater is detected.
  method CheckFor46PlusVersion(releaseKey: Integer): String;
  begin
    if releaseKey ≥ 528040 then 
    begin
      exit '4.8 or later';
    end;

    if releaseKey ≥ 461808 then 
    begin
      exit '4.7.2';
    end;

    if releaseKey ≥ 461308 then 
    begin
      exit '4.7.1';
    end;

    if releaseKey ≥ 460798 then 
    begin
      exit '4.7';
    end;

    if releaseKey ≥ 394802 then 
    begin
      exit '4.6.2';
    end;

    if releaseKey ≥ 394254 then 
    begin
      exit '4.6.1';
    end;

    if releaseKey ≥ 393295 then 
    begin
      exit '4.6';
    end;
  
    //  This code should never execute. 
    exit 'Error -- version 4.6 or later is not detected!';
  end;
  
  // Check the Registry for the .NET Framework version. This will work for .NET 4.5 and greater,
  // but GSD apps target 4.6 and greater so we check for that.
  var subkey: String := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\'; readonly;
  using ndpKey := RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(subkey) do 
  begin
    if assigned(ndpKey) and assigned(ndpKey.GetValue('Release')) then 
    begin
      result := CheckFor46PlusVersion(Integer(ndpKey.GetValue('Release')));
    end
    else
    begin
      result := 'Error -- v4 version information not found!';
    end;
  end;
end;

method ExceptionWriter.get_ScreenResolution: String;
begin
  result := 'Info not available';
  try
    var searcher := new ManagementObjectSearcher('select CurrentHorizontalResolution, CurrentVerticalResolution from Win32_VideoController');
    var managementObjects := searcher.Get();
    var res := String.Empty;
    for each mo: ManagementObject in managementObjects index i do
    begin
       res := res + iif(i > 0, ' | ', '') +  mo['CurrentHorizontalResolution'].ToString() + ' * ' + mo['CurrentVerticalResolution'].ToString();
    end;
    exit res;
  except
  end;
end;

end.