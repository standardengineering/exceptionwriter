﻿namespace Essy.Tools;

interface

uses
  System.Collections.Generic,
  System.Windows,
  System.Windows.Controls,
  System.Windows.Data,
  System.Windows.Documents,
  System.Windows.Media,
  System.Windows.Navigation,
  System.Windows.Shapes,
  System.Text,
  System.IO;

type
  InfoFormWPF = public partial class(Window)
  private
    fErrorMsg: String;
    fFilePath: String;
    method Window_Loaded(sender: Object; e: System.Windows.RoutedEventArgs);
    method btnContinue_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    method btnQuit_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    method btnOpenFile_Click(sender: Object; e: System.Windows.RoutedEventArgs);
    fDontUseContinueButton: Boolean;
  public
    constructor;
    method ShowDialog(aErrorMsg: String; aFilePath: String; aDontUseContinueButton: Boolean) : Boolean;
  end;
  
implementation

constructor InfoFormWPF;
begin
  InitializeComponent();
end;

method InfoFormWPF.Window_Loaded(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  var split := fFilePath.Split([Path.DirectorySeparatorChar]);
  var newFileName := new System.Text.StringBuilder();
  var prevLineNumber := 0;
  for each str in split index indx do
  begin
    var lineNumber := (newFileName.Length + 1 + str.Length) div 75;
    if lineNumber <> prevLineNumber then
    begin
      prevLineNumber := lineNumber;
      newFileName.AppendLine();
    end;
    if indx > 0 then newFileName.Append(Path.DirectorySeparatorChar);
    newFileName.Append(str);
  end;
  btnContinue.Visibility := iif(fDontUseContinueButton,Visibility.Collapsed);
  tbContinuePrompt.Visibility := iif(fDontUseContinueButton,Visibility.Collapsed);
  tbErrorText.Text := fErrorMsg;
  tbFileText.Text := newFileName.ToString();
end;

method InfoFormWPF.btnContinue_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  self.DialogResult := true;
  self.Close();
end;

method InfoFormWPF.btnQuit_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  self.DialogResult := false;
  self.Close();
end;

method InfoFormWPF.btnOpenFile_Click(sender: Object; e: System.Windows.RoutedEventArgs);
begin
  System.Diagnostics.Process.Start(fFilePath);
end;

method InfoFormWPF.ShowDialog(aErrorMsg: String; aFilePath: String; aDontUseContinueButton: Boolean) : Boolean;
begin
  fDontUseContinueButton := aDontUseContinueButton;
  fErrorMsg := aErrorMsg;
  fFilePath := aFilePath;
  result := self.ShowDialog();
end;
  
end.
