﻿namespace Essy.Tools;

interface

uses
  System.Drawing,
  System.Collections,
  System.Windows.Forms,
  System.Collections.Generic,
  System.Reflection,
  System.ComponentModel;

type
  InfoForm = partial class
  {$REGION Windows Form Designer generated fields}
  private
  {$REGION Windows Form Designer generated code}
    
  {$ENDREGION}
  {$region Windows Form Designer generated code}
    lblContactUs: System.Windows.Forms.Label;
  {$endregion}
  {$region Windows Form Designer generated code}
    lblContinueQuit: System.Windows.Forms.Label;
  {$endregion}
  {$region Windows Form Designer generated code}
    btnContinue: System.Windows.Forms.Button;
  {$endregion}
  {$region Windows Form Designer generated code}
    lblFileNamePrompt: System.Windows.Forms.Label;
  {$endregion}
  {$region Windows Form Designer generated code}
    lblErrorMsgPrompt: System.Windows.Forms.Label;
  {$endregion}
  {$region Windows Form Designer generated code}
    lblErrorMsg: System.Windows.Forms.Label;
    flowLayoutPanel1: System.Windows.Forms.FlowLayoutPanel;
  {$endregion}
    var components: System.ComponentModel.Container := nil;
    pictureBox1: System.Windows.Forms.PictureBox;
    btnOpenFile: System.Windows.Forms.Button;
    lblFileName: System.Windows.Forms.Label;
    
    btnClose: System.Windows.Forms.Button;
    method InitializeComponent;
  {$ENDREGION}
  end;

implementation

{$REGION Windows Form Designer generated code}
method InfoForm.InitializeComponent;
begin
  self.btnClose := new System.Windows.Forms.Button();
  self.lblFileName := new System.Windows.Forms.Label();
  self.btnOpenFile := new System.Windows.Forms.Button();
  self.pictureBox1 := new System.Windows.Forms.PictureBox();
  self.lblFileNamePrompt := new System.Windows.Forms.Label();
  self.btnContinue := new System.Windows.Forms.Button();
  self.lblContinueQuit := new System.Windows.Forms.Label();
  self.lblContactUs := new System.Windows.Forms.Label();
  self.lblErrorMsgPrompt := new System.Windows.Forms.Label();
  self.lblErrorMsg := new System.Windows.Forms.Label();
  self.flowLayoutPanel1 := new System.Windows.Forms.FlowLayoutPanel();
  (self.pictureBox1 as System.ComponentModel.ISupportInitialize).BeginInit();
  self.flowLayoutPanel1.SuspendLayout();
  self.SuspendLayout();
  //  btnClose
  self.btnClose.Anchor := (System.Windows.Forms.AnchorStyles.Bottom or System.Windows.Forms.AnchorStyles.Right) as System.Windows.Forms.AnchorStyles;
  self.btnClose.DialogResult := System.Windows.Forms.DialogResult.Abort;
  self.btnClose.Location := new System.Drawing.Point(466, 227);
  self.btnClose.Name := 'btnClose';
  self.btnClose.Size := new System.Drawing.Size(75, 23);
  self.btnClose.TabIndex := 0;
  self.btnClose.Text := 'Quit';
  self.btnClose.UseVisualStyleBackColor := true;
  //  lblFileName
  self.lblFileName.AutoSize := true;
  self.lblFileName.Font := new System.Drawing.Font('Microsoft Sans Serif', 8.25, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0 as System.Byte);
  self.lblFileName.Location := new System.Drawing.Point(3, 60);
  self.lblFileName.MaximumSize := new System.Drawing.Size(434, 0);
  self.lblFileName.Name := 'lblFileName';
  self.lblFileName.Size := new System.Drawing.Size(146, 13);
  self.lblFileName.TabIndex := 2;
  self.lblFileName.Text := '>>>file name place holder<<<';
  //  btnOpenFile
  self.btnOpenFile.Anchor := (System.Windows.Forms.AnchorStyles.Bottom or System.Windows.Forms.AnchorStyles.Left) as System.Windows.Forms.AnchorStyles;
  self.btnOpenFile.Location := new System.Drawing.Point(66, 227);
  self.btnOpenFile.Name := 'btnOpenFile';
  self.btnOpenFile.Size := new System.Drawing.Size(107, 23);
  self.btnOpenFile.TabIndex := 3;
  self.btnOpenFile.Text := 'Open Log File';
  self.btnOpenFile.UseVisualStyleBackColor := true;
  self.btnOpenFile.Click += new System.EventHandler(@self.btnOpenFile_Click);
  //  pictureBox1
  self.pictureBox1.Image := Essy.Tools.Properties.Resources.Error;
  self.pictureBox1.Location := new System.Drawing.Point(12, 9);
  self.pictureBox1.Name := 'pictureBox1';
  self.pictureBox1.Size := new System.Drawing.Size(48, 49);
  self.pictureBox1.TabIndex := 4;
  self.pictureBox1.TabStop := false;
  //  lblFileNamePrompt
  self.lblFileNamePrompt.Location := new System.Drawing.Point(3, 42);
  self.lblFileNamePrompt.Margin := new System.Windows.Forms.Padding(3, 10, 3, 5);
  self.lblFileNamePrompt.Name := 'lblFileNamePrompt';
  self.lblFileNamePrompt.Size := new System.Drawing.Size(434, 13);
  self.lblFileNamePrompt.TabIndex := 6;
  self.lblFileNamePrompt.Text := 'A more detailed log of the error has been written to the following file:';
  //  btnContinue
  self.btnContinue.Anchor := (System.Windows.Forms.AnchorStyles.Bottom or System.Windows.Forms.AnchorStyles.Right) as System.Windows.Forms.AnchorStyles;
  self.btnContinue.DialogResult := System.Windows.Forms.DialogResult.Ignore;
  self.btnContinue.Location := new System.Drawing.Point(385, 227);
  self.btnContinue.Name := 'btnContinue';
  self.btnContinue.Size := new System.Drawing.Size(75, 23);
  self.btnContinue.TabIndex := 7;
  self.btnContinue.Text := 'Continue';
  self.btnContinue.UseVisualStyleBackColor := true;
  //  lblContinueQuit
  self.lblContinueQuit.Location := new System.Drawing.Point(3, 83);
  self.lblContinueQuit.Margin := new System.Windows.Forms.Padding(3, 10, 3, 0);
  self.lblContinueQuit.Name := 'lblContinueQuit';
  self.lblContinueQuit.Size := new System.Drawing.Size(434, 29);
  self.lblContinueQuit.TabIndex := 8;
  self.lblContinueQuit.Text := 'If you click Continue, the application will ignore this error and attempt to continue. If you click Quit, the application will close immediately.';
  //  lblContactUs
  self.lblContactUs.Location := new System.Drawing.Point(3, 122);
  self.lblContactUs.Margin := new System.Windows.Forms.Padding(3, 10, 3, 0);
  self.lblContactUs.Name := 'lblContactUs';
  self.lblContactUs.Size := new System.Drawing.Size(434, 30);
  self.lblContactUs.TabIndex := 9;
  self.lblContactUs.Text := 'Please contact us if the problem persists.';
  //  lblErrorMsgPrompt
  self.lblErrorMsgPrompt.Location := new System.Drawing.Point(3, 0);
  self.lblErrorMsgPrompt.Margin := new System.Windows.Forms.Padding(3, 0, 3, 5);
  self.lblErrorMsgPrompt.Name := 'lblErrorMsgPrompt';
  self.lblErrorMsgPrompt.Size := new System.Drawing.Size(434, 14);
  self.lblErrorMsgPrompt.TabIndex := 1;
  self.lblErrorMsgPrompt.Text := 'The application has encountered an unexpected error:';
  //  lblErrorMsg
  self.lblErrorMsg.AutoSize := true;
  self.lblErrorMsg.Location := new System.Drawing.Point(3, 19);
  self.lblErrorMsg.MaximumSize := new System.Drawing.Size(434, 0);
  self.lblErrorMsg.Name := 'lblErrorMsg';
  self.lblErrorMsg.Size := new System.Drawing.Size(147, 13);
  self.lblErrorMsg.TabIndex := 5;
  self.lblErrorMsg.Text := '>>>error msg place holder<<<';
  //  flowLayoutPanel1
  self.flowLayoutPanel1.AutoScroll := true;
  self.flowLayoutPanel1.Controls.&Add(self.lblErrorMsgPrompt);
  self.flowLayoutPanel1.Controls.&Add(self.lblErrorMsg);
  self.flowLayoutPanel1.Controls.&Add(self.lblFileNamePrompt);
  self.flowLayoutPanel1.Controls.&Add(self.lblFileName);
  self.flowLayoutPanel1.Controls.&Add(self.lblContinueQuit);
  self.flowLayoutPanel1.Controls.&Add(self.lblContactUs);
  self.flowLayoutPanel1.Location := new System.Drawing.Point(66, 9);
  self.flowLayoutPanel1.Name := 'flowLayoutPanel1';
  self.flowLayoutPanel1.Padding := new System.Windows.Forms.Padding(0, 0, 20, 0);
  self.flowLayoutPanel1.Size := new System.Drawing.Size(476, 210);
  self.flowLayoutPanel1.TabIndex := 11;
  //  InfoForm
  self.BackColor := System.Drawing.SystemColors.Window;
  self.ClientSize := new System.Drawing.Size(554, 262);
  self.ControlBox := false;
  self.Controls.&Add(self.flowLayoutPanel1);
  self.Controls.&Add(self.btnContinue);
  self.Controls.&Add(self.pictureBox1);
  self.Controls.&Add(self.btnOpenFile);
  self.Controls.&Add(self.btnClose);
  self.FormBorderStyle := System.Windows.Forms.FormBorderStyle.FixedDialog;
  self.HelpButton := true;
  self.Name := 'InfoForm';
  self.ShowIcon := false;
  self.ShowInTaskbar := false;
  self.StartPosition := System.Windows.Forms.FormStartPosition.CenterScreen;
  self.Text := 'Application Error';
  (self.pictureBox1 as System.ComponentModel.ISupportInitialize).EndInit();
  self.flowLayoutPanel1.ResumeLayout(false);
  self.flowLayoutPanel1.PerformLayout();
  self.ResumeLayout(false);
end;
{$ENDREGION}

end.
