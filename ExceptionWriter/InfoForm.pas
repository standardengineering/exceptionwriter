﻿namespace Essy.Tools;

interface

uses
  System.Drawing,
  System.Collections,
  System.Windows.Forms,
  System.Collections.Generic,
  System.Reflection,
  System.IO,
  System.ComponentModel;

type
  /// <summary>
  /// Summary description for InfoForm.
  /// </summary>
  InfoForm = partial class(System.Windows.Forms.Form)
  private
  {$REGION designer-generated code}
    
  {$ENDREGION}
    fErrorMsg: String;
    fFileName: String;
    method btnOpenFile_Click(sender: System.Object; e: System.EventArgs);
  protected
    method Dispose(aDisposing: Boolean); override;
  public
    constructor;
    method ShowDialog(aErrorMsg: String; aFileName: String; aDontUseContinueButton: Boolean): DialogResult;
  end;

implementation

{$REGION Construction and Disposition}
constructor InfoForm;
begin
  //
  // Required for Windows Form Designer support
  //
  InitializeComponent();

  //
  // TODO: Add any constructor code after InitializeComponent call
  //
end;

method InfoForm.Dispose(aDisposing: Boolean);
begin
  if aDisposing then begin
    if assigned(components) then
      components.Dispose();

    //
    // TODO: Add custom disposition code here
    //
  end;
  inherited Dispose(aDisposing);
end;
{$ENDREGION}

method InfoForm.ShowDialog(aErrorMsg: String; aFilePath: String; aDontUseContinueButton: Boolean): DialogResult;
begin
  self.fErrorMsg := aErrorMsg;
  self.fFileName := aFileName;
  var split := aFileName.Split([Path.DirectorySeparatorChar]);
  var newFileName := new System.Text.StringBuilder();
  var prevLineNumber := 0;
  for each str in split index indx do
  begin
    var lineNumber := (newFileName.Length + 1 + str.Length) div 75;
    if lineNumber <> prevLineNumber then
    begin
      prevLineNumber := lineNumber;
      newFileName.AppendLine();
    end;
    if indx > 0 then newFileName.Append(Path.DirectorySeparatorChar);
    newFileName.Append(str);
  end;
  self.btnContinue.Visible := iif(aDontUseContinueButton,false,true);
  self.lblContinueQuit.Visible := iif(aDontUseContinueButton,false,true);
  self.lblErrorMsg.Text := fErrorMsg;
  self.lblFileName.Text := newFileName.ToString();
  result := self.ShowDialog();
end;

method InfoForm.btnOpenFile_Click(sender: System.Object; e: System.EventArgs);
begin
  System.Diagnostics.Process.Start(self.fFileName);
end;

{$REGION designer-generated code}

{$ENDREGION}



end.
