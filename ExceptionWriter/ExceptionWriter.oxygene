﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <ProductVersion>3.5</ProductVersion>
    <RootNamespace>Essy.Tools</RootNamespace>
    <OutputType>Library</OutputType>
    <AssemblyName>ExceptionWriter</AssemblyName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyWith>False</AllowLegacyWith>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <AllowUnsafeCode>False</AllowUnsafeCode>
    <Configuration Condition="'$(Configuration)' == ''">Debug</Configuration>
    <Platform Condition="'$(Platform)' == ''">AnyCPU</Platform>
    <TargetFrameworkVersion>v4.8</TargetFrameworkVersion>
    <Name>ExceptionWriter</Name>
    <ProjectGuid>{AD1DD47A-7397-4EF7-9899-4A5651622E34}</ProjectGuid>
    <ProjectView>ShowAllFiles</ProjectView>
    <Mode>Echoes</Mode>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Debug'">
    <OutputPath>.\bin\Debug\</OutputPath>
    <Optimize>False</Optimize>
    <DefineConstants>DEBUG;TRACE;</DefineConstants>
    <GeneratePDB>True</GeneratePDB>
    <CaptureConsoleOutput>False</CaptureConsoleOutput>
    <StartMode>Project</StartMode>
    <CpuType>anycpu</CpuType>
    <RuntimeVersion>v25</RuntimeVersion>
    <XmlDoc>False</XmlDoc>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <EnableUnmanagedDebugging>False</EnableUnmanagedDebugging>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Release'">
    <OutputPath>.\bin\Release\</OutputPath>
    <Optimize>True</Optimize>
    <GeneratePDB>True</GeneratePDB>
    <GenerateMDB>False</GenerateMDB>
    <EnableAsserts>False</EnableAsserts>
    <TreatWarningsAsErrors>False</TreatWarningsAsErrors>
    <CaptureConsoleOutput>False</CaptureConsoleOutput>
    <StartMode>Project</StartMode>
    <RegisterForComInterop>False</RegisterForComInterop>
    <CpuType>anycpu</CpuType>
    <RuntimeVersion>v25</RuntimeVersion>
    <XmlDoc>False</XmlDoc>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <EnableUnmanagedDebugging>False</EnableUnmanagedDebugging>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="ControlzEx">
      <HintPath>N:\ControlzEx.3.0.2.4\lib\net45\ControlzEx.dll</HintPath>
      <Private>True</Private>
    </Reference>
    <Reference Include="MahApps.Metro">
      <HintPath>N:\MahApps.Metro.1.6.5\lib\net46\MahApps.Metro.dll</HintPath>
      <Private>True</Private>
    </Reference>
    <Reference Include="Microsoft.VisualBasic">
      <HintPath>C:\Windows\Microsoft.NET\Framework\v2.0.50727\Microsoft.VisualBasic.dll</HintPath>
    </Reference>
    <Reference Include="mscorlib" />
    <Reference Include="PresentationCore">
      <HintPath>C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\PresentationCore.dll</HintPath>
    </Reference>
    <Reference Include="PresentationFramework">
      <HintPath>C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\PresentationFramework.dll</HintPath>
    </Reference>
    <Reference Include="System" />
    <Reference Include="System.Data" />
    <Reference Include="System.Drawing" />
    <Reference Include="System.Windows.Interactivity">
      <HintPath>N:\ControlzEx.3.0.2.4\lib\net45\System.Windows.Interactivity.dll</HintPath>
      <Private>True</Private>
    </Reference>
    <Reference Include="System.Xaml">
      <HintPath>C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.0\System.Xaml.dll</HintPath>
    </Reference>
    <Reference Include="System.Xml" />
    <Reference Include="System.Core">
      <RequiredTargetFramework>3.5</RequiredTargetFramework>
    </Reference>
    <Reference Include="System.Xml.Linq">
      <RequiredTargetFramework>3.5</RequiredTargetFramework>
    </Reference>
    <Reference Include="System.Data.DataSetExtensions">
      <RequiredTargetFramework>3.5</RequiredTargetFramework>
    </Reference>
    <Reference Include="WindowsBase" />
    <Reference Include="System.Windows.Forms" />
    <Reference Include="System.Management" />
  </ItemGroup>
  <ItemGroup>
    <Compile Include="ExceptionWriter.pas" />
    <Compile Include="InfoForm.Designer.pas">
      <SubType>Form</SubType>
      <DependentUpon>InfoForm.pas</DependentUpon>
      <DesignableClassName>Essy.Tools.InfoForm</DesignableClassName>
    </Compile>
    <Compile Include="InfoForm.pas">
      <DesignableClassName>Essy.Tools.InfoForm</DesignableClassName>
      <SubType>Form</SubType>
    </Compile>
    <Compile Include="InfoFormMetro.xaml.pas">
      <DependentUpon>InfoFormMetro.xaml</DependentUpon>
    </Compile>
    <Compile Include="InfoFormWPF.xaml.pas">
      <DependentUpon>InfoFormWPF.xaml</DependentUpon>
    </Compile>
    <Compile Include="Properties\AssemblyInfo.pas" />
    <Compile Include="Properties\Resources.Designer.pas" />
    <EmbeddedResource Include="InfoForm.resx">
      <DependentUpon>InfoForm.pas</DependentUpon>
    </EmbeddedResource>
    <EmbeddedResource Include="Properties\Resources.cs.resx" />
    <EmbeddedResource Include="Properties\Resources.de.resx" />
    <EmbeddedResource Include="Properties\Resources.es.resx" />
    <EmbeddedResource Include="Properties\Resources.fr.resx" />
    <EmbeddedResource Include="Properties\Resources.it.resx" />
    <EmbeddedResource Include="Properties\Resources.pt-br.resx" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
    <Page Include="InfoFormMetro.xaml" />
    <Page Include="InfoFormWPF.xaml" />
    <Content Include="packages.config">
      <SubType>Content</SubType>
    </Content>
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.Legacy.targets" />
</Project>